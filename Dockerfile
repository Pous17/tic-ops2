FROM python:3.8-slim

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 5432

CMD ["python","base.py","database.py"]